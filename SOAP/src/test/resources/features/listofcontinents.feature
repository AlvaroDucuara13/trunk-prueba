# language: es
@FeatureName: nombresDeContinentes
Característica: Obtener nombres de continentes
  Como analista geopolítico
  Quiero conocer los nombres y códigos ISO de los continentes
  Para poder realizar análisis de dinámicas políticas inter y transcontinentales

  @ScenarioName: listarNombresDeContinentes

  Escenario: Listar nombres y codigos ISO de continentes
    Dado que el analista geopolitico ha ingresado al recurso de listar nombres
    Cuando solicite el listado de nombres de los continentes
    Entonces obtendra el listado de los codigos ISO y nombres de los continentes
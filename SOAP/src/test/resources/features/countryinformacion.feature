 # language: es



 @FeatureName:Fullcountryinfo
 Característica: Como usuario de la pagina DataFlex
   necesito consultar con el código ISO del pais
   para obtener la informacion completa de un pais.


   @ScenarioName:Validarelcodigoisoestecorrecto
   Escenario: Validar el codigo ISO este correcto
     Dado que el usuario necesita consultar la informacion completa de un pais, con el codigo ISO
     Cuando el usuario mediante el codigo ISO "COL" ejecuta una consulta
     Entonces el ususario deberia obtener toda la informacion del pais que esta consultando

   @ScenarioName:Validarelcodigoisoesteincorrecto
   Escenario: Validar el codigo ISO este incorrecto
     Dado que el usuario ingresa un codigo ISO no valido "FALSE"
     Cuando el usuario realiza una consulta desde la pagina
     Entonces el usuario deberia obtener como respuesta: "Country not found in the database"
package co.com.sofka.util;

public enum CapitalCityKey {
    ISO_CODE("[ISOCode]");

    private final String value;

    CapitalCityKey(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

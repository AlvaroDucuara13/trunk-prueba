package co.com.sofka.question.listofcontinents;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;


public class TheListOfContinents implements Question {
    @Override
    public String answeredBy(Actor actor) {
        return SerenityRest.lastResponse().asString();
    }

    public static TheListOfContinents theListOfContinents() {
        return new TheListOfContinents();
    }
}

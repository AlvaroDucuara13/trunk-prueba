package co.com.sofka.question.capitalcitys;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class CapitalCitysQuest implements Question {

    public static CapitalCitysQuest capitalCitysQuest(){
        return new CapitalCitysQuest();
    }

    @Override
    public String answeredBy(Actor actor) {
        return SerenityRest.lastResponse().asString();
    }
}

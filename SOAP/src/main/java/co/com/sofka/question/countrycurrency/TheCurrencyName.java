package co.com.sofka.question.countrycurrency;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class TheCurrencyName implements Question {
    private ObjectMapper objectMapper = new XmlMapper();

    @Override
    public String answeredBy(Actor actor) {

        return SerenityRest.lastResponse().asString();
    }

    public static TheCurrencyName theCountryCurrency(){
        return new TheCurrencyName();
    }
}

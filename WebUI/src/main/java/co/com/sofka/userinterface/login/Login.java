package co.com.sofka.userinterface.login;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class Login extends PageObject {
    public static final Target SING_IN = Target
            .the("Singin")
            .located(By.xpath("//button[contains(text(),'Log in')]"));

    public static final Target USER_NAME = Target
            .the("UserName")
            .located(By.xpath("//body/div[@id='app']/div[1]/section[3]/div[1]/div[2]/div[1]/form[1]/div[1]/input[1]"));

    public static final Target PASSWORDLOGIN = Target
            .the("Password")
            .located(By.xpath("//body/div[@id='app']/div[1]/section[3]/div[1]/div[2]/div[1]/form[1]/div[2]/input[1]"));

    public static final Target BTN_LOGIN = Target
            .the("BTN_Login")
            .located(By.xpath("//body/div[@id='app']/div[1]/section[3]/div[1]/div[2]/div[1]/nav[1]/button[2]"));

}

package co.com.sofka.userinterface.loadmorebutton;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class LoadMoreButton extends PageObject {

    public static final Target LOAD_MORE = Target
            .the("LoadMore")
            .located(By.xpath("//div[5]/button"));

    public static final Target CUOZHOU = Target
            .the("cuozhou nombre")
            .located(By.xpath("//div[9]/div[2]/div/h5"));

    public static final Target BOOK_BUTTON = Target
            .the("cuozhou book")
            .located(By.xpath("//div[9]/div[4]/button"));

    public static final Target TEMP_MESSAGE = Target
            .the("cuozhou temp message")
            .located(By.xpath("//div[@id='app']/div/div[2]/section[2]/div/h1"));

}

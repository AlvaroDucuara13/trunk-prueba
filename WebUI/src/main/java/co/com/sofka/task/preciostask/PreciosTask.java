package co.com.sofka.task.preciostask;

import co.com.sofka.userinterface.precios.PreciosPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.DoubleClick;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.SendKeys;
import org.openqa.selenium.Keys;

public class PreciosTask implements Task {

    private String Presupuesto;

    public PreciosTask Presupuesto(String presupuesto) {
        Presupuesto = presupuesto;
        return this;
    }

    public static PreciosTask pruebatask() {
        return new PreciosTask();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Scroll.to(PreciosPage.TITULO_ROLL),
                DoubleClick.on(PreciosPage.CAMPO_PRICE),
                SendKeys.of(Presupuesto).into(PreciosPage.CAMPO_PRICE),
                Hit.the(Keys.ENTER).into(PreciosPage.CAMPO_PRICE),
                Scroll.to(PreciosPage.CAMPO_VALIDATION)

        );
    }
}

package co.com.sofka.task.login;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;


import static co.com.sofka.userinterface.login.Login.SING_IN;

public class NavegateToLogin implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(

                Click.on(SING_IN)
        );
    }
    public static NavegateToLogin navegateToLogin(){
        return new NavegateToLogin();
    }
}

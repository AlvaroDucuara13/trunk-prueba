package co.com.sofka.task.login;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;

import static co.com.sofka.userinterface.login.Login.*;

public class LlenarCamposLogin implements Task {

    private String EmailAddressLogin;
    private String PasswordLogin;

    public LlenarCamposLogin EmailAddressLogin(String emailAddressLogin) {
        EmailAddressLogin = emailAddressLogin;
        return this;
    }

    public LlenarCamposLogin PasswordLogin(String passwordLogin) {
        PasswordLogin = passwordLogin;
        return this;
    }



    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
        Scroll.to(USER_NAME),
                Enter.theValue(EmailAddressLogin).into(USER_NAME),

                Scroll.to(PASSWORDLOGIN),
                Enter.theValue(PasswordLogin).into(PASSWORDLOGIN),

                Click.on(BTN_LOGIN)
);
    }
    public static LlenarCamposLogin llenarCamposLogin(){
        return new LlenarCamposLogin();
    }
}

package co.com.sofka.question;

import co.com.sofka.question.Mensaje;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.TextContent;

public class ResumeLogin {
    public static Question<String> Message(){
        return actor -> TextContent.of(Mensaje.Message).viewedBy(actor).asString().trim();
    }
    public static Question<String> MessageLoginFailed(){
        return actor -> TextContent.of(Mensaje.MessageLoginFailed).viewedBy(actor).asString().trim();
    }
}

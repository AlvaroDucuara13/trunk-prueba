package co.com.sofka.question;


import org.openqa.selenium.By;

public class Mensaje {
    public static By Message = By.xpath("//body[1]/div[1]/div[1]/header[1]/div[1]/div[2]/ul[1]/div[1]/button[1]/span[1]");
    public static By MessageLoginFailed = By.xpath("//span[contains(text(),'Password is a required field.')]");
}

package co.com.sofka.util;

import co.com.sofka.model.checkout.CheckoutModel;
import com.github.javafaker.Faker;

import java.util.Locale;
import java.util.Random;

public class Helper {

    public static CheckoutModel generarCheckout(String language, String country){
        Faker faker =  Faker.instance(
                new Locale(language, country),
                new Random()
        );

        CheckoutModel checkoutModel = new CheckoutModel();
        checkoutModel.setName(faker.name().firstName());
        checkoutModel.setEmail(faker.internet().emailAddress());
        checkoutModel.setSnn(faker.idNumber().ssnValid());
        checkoutModel.setPhone(faker.phoneNumber().phoneNumber());

        return checkoutModel;
    }
}

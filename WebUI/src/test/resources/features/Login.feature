#language: es

 Característica: funcionalidad Login
  Yo como usuario
  Quiero poder loguearme en la web
  Para  navegar dentro de ella

 Escenario: inicio de sesion exitoso
   Dado que el usuario se encuentra en la pagina de inicio de sesion
   Cuando el usuario ingresa un nombre de usuario y una contrasena validos
   Entonces el usuario debe tener acceso a la pagina de inicio

 Escenario: inicio de sesion fallido
   Dado que el usuario quiere iniciar sesion
   Cuando el usuario no ingresa un password
   Entonces el usuario espera ver un mensaje de autenticacion fallida
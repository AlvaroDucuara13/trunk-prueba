# language: es
@FeatureName:checkout
Característica: checkout
  Yo como usuario
  Quiero poder hacer el checkout
  Para realizar la compra

  @ScenarioName:AceptarTerminosYCondiciones
  Escenario: Aceptar terminos y condiciones
    Dado que el usuario reservo un viaje
    Cuando intenta pagar sin aceptar los terminos y condiciones
    Entonces el usuario vera en pantalla una alerta

package co.com.sofka.stepdefinitions.setup;

import org.apache.log4j.PropertyConfigurator;

import java.util.Locale;

import static co.com.sofka.util.Log4jValues.LOG4J_LINUX_PROPERTIES_FILE_PATH;
import static co.com.sofka.util.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;
import static com.google.common.base.StandardSystemProperty.USER_DIR;

public class Log4jPath {




        protected void setup()
        {
            String os = System.getProperty("os.name").toLowerCase(Locale.ROOT).substring(0,3);
            switch(os)
            {
                case "lin":
                    PropertyConfigurator.configure(USER_DIR.value() + LOG4J_LINUX_PROPERTIES_FILE_PATH.getValue() );
                    break;

                case "win":
                    PropertyConfigurator.configure(USER_DIR.value() + LOG4J_PROPERTIES_FILE_PATH.getValue() );
                    break;
            }

        }
}

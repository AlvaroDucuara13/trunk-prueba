package co.com.sofka.stepdefinitions;

import co.com.sofka.model.actualizarDatosRespuesta;
import co.com.sofka.model.datosModelActualizarDatos;
import co.com.sofka.question.actualizarDatosQuestion;
import co.com.sofka.stepdefinitions.setup.Log4jPath;
import co.com.sofka.util.leerArchivoJson;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import static co.com.sofka.task.doActualizarDatos.doPatch;
import static co.com.sofka.util.Dictionary.RESOURCE;
import static co.com.sofka.util.Dictionary.URLBASE;
import static co.com.sofka.util.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;
import static co.com.sofka.util.Log4jValues.USER_DIR;
import static co.com.sofka.util.Persona.generarPersonasRandom;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.equalTo;

public class actualizarDatosStep extends Log4jPath {


    private final Actor actor = Actor.named("AlvaroD");
    private String bodyRequest;
    private datosModelActualizarDatos datosRandom;

    private leerArchivoJson archvioJson;
    private static final Logger LOGGER = Logger.getLogger(actualizarDatosStep.class);

    @Dado("el usuario esta en la plataforma y desea actualizar datos")
    public void elUsuarioEstaEnLaPlataformaYDeseaActualizarDatos() {
        setup();

        try{
            PropertyConfigurator.configure(USER_DIR.getValue() + LOG4J_PROPERTIES_FILE_PATH.getValue());
            actor.can(CallAnApi.at(URLBASE));


            datosRandom = generarPersonasRandom();
            archvioJson = new leerArchivoJson(datosRandom.getJob(), datosRandom.getFullName() );
            String SaveNameJob = archvioJson.ConvertPatch();
            bodyRequest = SaveNameJob;
            LOGGER.info(bodyRequest);

        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);

        }
    }

    @Cuando("el usuario logra actualizar los datos")
    public void elUsuarioLograActualizarLosDatos() {

        try{

            actor.attemptsTo(
                    doPatch()
                            .usingTheResource(RESOURCE)
                            .andBodyRequest(bodyRequest)
            );

        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);

        }

    }

    @Entonces("el usuario obtendra un codigo de respuesta exitosa y podra ver sus datos actualizados")
    public void elUsuarioObtendraUnCodigoDeRespuestaExitosaYPodraVerSusDatosActualizados() {

        try{

            LastResponse.received().answeredBy(actor).prettyPrint();

            actualizarDatosRespuesta Customer = new actualizarDatosQuestion().answeredBy(actor);
            actor.should(
                    seeThatResponse(
                            "El código de respuesta debe ser: " + HttpStatus.SC_OK,
                            validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)),
                    seeThat(
                            "El nombre del usuario es:", x -> Customer.getName(), equalTo(datosRandom.getFullName())),
                    seeThat(
                            "El trabajo del usuario es:", x -> Customer.getJob(), equalTo(datosRandom.getJob()))
            );

        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);

        }

    }
}

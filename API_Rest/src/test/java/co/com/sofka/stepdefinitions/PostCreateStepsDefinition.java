package co.com.sofka.stepdefinitions;

import co.com.sofka.model.actualizarDatosRespuesta;
import co.com.sofka.model.createRespuesta;
import co.com.sofka.model.datosModelActualizarDatos;
import co.com.sofka.question.actualizarDatosQuestion;
import co.com.sofka.question.createQuestion;
import co.com.sofka.stepdefinitions.setup.Log4jPath;
import co.com.sofka.util.leerArchivoJson;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Consequence;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import static co.com.sofka.task.doActualizarDatos.doPatch;
import static co.com.sofka.util.Dictionary.*;
import static co.com.sofka.util.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;
import static co.com.sofka.util.Log4jValues.USER_DIR;
import static co.com.sofka.util.Persona.generarPersonasRandom;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;

public class PostCreateStepsDefinition extends Log4jPath {
    private final Actor actor = Actor.named("JhonS");
    private String bodyRequest;
    private datosModelActualizarDatos datosRandom;
    private leerArchivoJson archvioJson;
    private static final Logger LOGGER = Logger.getLogger(PostCreateStepsDefinition.class);

    @Dado("se crea nuevo puesto por post")
    public void seCreaNuevoPuestoPorPost(){
        setup();

        try{
            PropertyConfigurator.configure(USER_DIR.getValue() + LOG4J_PROPERTIES_FILE_PATH.getValue());
            actor.can(CallAnApi.at(URLBASE));
            datosRandom = generarPersonasRandom();
            archvioJson = new leerArchivoJson(datosRandom.getJob(), datosRandom.getFullName());
            String SaveNameJob = archvioJson.ConvertPatch();
            bodyRequest = SaveNameJob;
            LOGGER.info(bodyRequest);
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Cuando("se solicita por request")
    public void seSolicitaPorRequest(){
        try{
            actor.attemptsTo(
                    doPatch()
                            .usingTheResource(CREATE)
                            .andBodyRequest(bodyRequest)
            );
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("se obtiene respuesta 200 con id")
    public void seObtieneRespuesta200ConId(){
        try{
            LastResponse.received().answeredBy(actor).prettyPrint();
            createRespuesta User = new createQuestion().answeredBy(actor);
            actor.should(
                    seeThatResponse(
                            "La respuesta esperada es: " + HttpStatus.SC_CREATED,
                            validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_CREATED)),
                    seeThat("El nombre el Usuario es: ", x -> User.getName(), equalTo(datosRandom.getFullName())),
                    seeThat("El trabajo del Usuario es: ", x -> User.getJob(), equalTo(datosRandom.getJob())),
                    seeThat("El ID del Usuario es: ", x -> User.getId(), notNullValue())
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }
}

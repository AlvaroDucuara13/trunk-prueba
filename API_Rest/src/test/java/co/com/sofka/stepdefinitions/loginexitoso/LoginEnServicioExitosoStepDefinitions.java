package co.com.sofka.stepdefinitions.loginexitoso;

import co.com.sofka.question.GetToken;
import co.com.sofka.stepdefinitions.setup.Log4jPath;
import co.com.sofka.util.FileReader;
import co.com.sofka.util.leerArchivoJson;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.hamcrest.Matchers;
import org.json.simple.parser.ParseException;

import java.io.IOException;

import static co.com.sofka.task.login.DoPostLogin.*;
import static co.com.sofka.util.Dictionary.RESOURCE_LOGIN;
import static co.com.sofka.util.Dictionary.URLBASE;
import static co.com.sofka.util.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;
import static co.com.sofka.util.Log4jValues.USER_DIR;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;


public class LoginEnServicioExitosoStepDefinitions extends Log4jPath {
    private final Actor actor = Actor.named("JohanM");
    private String bodyRequest;
    private FileReader fileReader;

    private leerArchivoJson archivoJson;
    private static final Logger LOGGER = Logger.getLogger(LoginEnServicioExitosoStepDefinitions.class);

    @Dado("el usuario esta en la plataforma y desea loguearse en el servicio")
    public void elUsuarioEstaEnLaPlataformaYDeseaLoguearseEnElServicio() throws IOException, ParseException {
        setup();

        PropertyConfigurator.configure(USER_DIR.getValue() + LOG4J_PROPERTIES_FILE_PATH.getValue());
        actor.can(CallAnApi.at(URLBASE));

    }

    @Cuando("el usuario logra loguearse con sus credenciales email {string} y password {string}")
    public void elUsuarioLograLoguearseConSusCredencialesEmailYPassword(String email, String password) {
        fileReader = new FileReader("PostLoginSuccesfully.json");
        String SaveNameJob = fileReader.readContent().replace("[email]", email).replace("[password]", password);
        bodyRequest = SaveNameJob;
        LOGGER.info(bodyRequest);
        actor.attemptsTo(
                doPostLogin()
                        .usingTheResource(RESOURCE_LOGIN)
                        .andBodyRequest(bodyRequest));
    }

    @Entonces("el usuario obtendra un codigo de respuesta exitosa y podra ver su token de acceso")
    public void elUsuarioObtendraUnCodigoDeRespuestaExitosaYPodraVerSuTokenDeAcceso() {
        LastResponse.received().answeredBy(actor).prettyPrint();
        actor.should(seeThat(GetToken.getToken(), Matchers.containsString("token")));


    }
}
package co.com.sofka.stepdefinitions;

import co.com.sofka.question.SingleResourceQuestion;
import co.com.sofka.stepdefinitions.setup.Log4jPath;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.hamcrest.Matchers;

import static co.com.sofka.task.GetColorData.getColorData;
import static co.com.sofka.util.Dictionary.SINGLE_RESOURCE;
import static co.com.sofka.util.Dictionary.URLBASE;
import static co.com.sofka.util.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;
import static co.com.sofka.util.Log4jValues.USER_DIR;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class SingleResourceStepDefinition extends Log4jPath  {


    private final Actor actor = Actor.named("DanielaG");
    private String bodyRequest;

    private static final Logger LOGGER = Logger.getLogger(SingleResourceStepDefinition.class);

    @Dado("que estoy en el servicio de paleta de colores")
    public void queEstoyEnElServicioDePaletaDeColores() {
        setup();

        try{
            PropertyConfigurator.configure(USER_DIR.getValue() + LOG4J_PROPERTIES_FILE_PATH.getValue());
            actor.can(CallAnApi.at(URLBASE));


        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

        }

    @Cuando("solicite el codigo de color fucsia")
    public void soliciteElCodigoDeColorFucsia() {

        try {
        actor.attemptsTo(getColorData().usetheresource(SINGLE_RESOURCE));


        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("se obtendra un status {int} y el codigo correspondiente")
    public void seObtendraUnStatusYElCodigoCorrespondiente(Integer statusCode) {

         String codigoColor = SingleResourceQuestion.singleResourceQuestion().answeredBy(actor).getData().getColor();

         actor.should(
                 seeThatResponse(
                         "El código de respuesta debe ser: " + statusCode,
                         validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)),
                 seeThat(
                         "El codigo del color es",actor1 -> codigoColor, Matchers.notNullValue()
                 )

         );
    }
}

package co.com.sofka.util;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

import static co.com.sofka.util.palabrasClaves.COMPLETENAME;
import static co.com.sofka.util.palabrasClaves.JOB;


public class leerArchivoJson {
    
    private static String FullName;
    private static String Job;

    private static String FullJson;
    private static Object JsonObjc;
    private static JSONObject JsonObjc2;


    private static String SaveFullname;
    private static String SaveJob;



    //Contructor

    public leerArchivoJson(String job, String fullName) {
        this.Job = job;
        this.FullName = fullName;
    }

    //lectura del archvio Json
    
    public static String ConvertPatch() throws IOException, ParseException {
        JsonObjc = new JSONParser().parse(new FileReader("src/test/resources/ArchivosJson/PatchCustomer.json"));
        JsonObjc2 = (JSONObject) JsonObjc;
        FullJson = JSONObject.toJSONString(JsonObjc2);
        SaveFullname = FullJson.replace(COMPLETENAME.getValue(), FullName);
        SaveJob =SaveFullname.replace(JOB.getValue(), Job);
        return SaveJob;
    }

}

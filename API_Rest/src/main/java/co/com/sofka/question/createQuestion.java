package co.com.sofka.question;

import co.com.sofka.model.createRespuesta;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class createQuestion implements Question {

    @Override
    public createRespuesta answeredBy(Actor actor){
        return SerenityRest.lastResponse().as(createRespuesta.class);
    }
}
